﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eShop2._0.Startup))]
namespace eShop2._0
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
